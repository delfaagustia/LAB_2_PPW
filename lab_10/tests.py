from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .custom_auth import auth_login, auth_logout
from .csui_helper import get_access_token, get_data_user
from .omdb_api import search_movie, get_detail_movie
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class Lab10UnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")


    def test_lab_10_url_is_exist(self):
        response = Client().get('/lab-10/')
        self.assertEqual(response.status_code,200)

    def test_lab_10_using_index_func(self):
        found = resolve('/lab-10/')
        self.assertEqual(found.func, index)

    def test_lab_10_template_using_lab_10_html(self):
        response = Client().get('/lab-10/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,  "lab_10/login.html")
        
        response = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('lab_10/dashboard.html')

    def test_login_wrong_user_pass(self):#line 29
        response = self.client.post('/lab-10/custom_auth/login/', {'username': "delfa", 'password': "delfaag"})
       # self.assertEqual(response, "Username atau password salah")
        self.client.get('/lab-10/').content.decode('utf-8')

    def test_logout(self):
        response = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.post('/lab-10/custom_auth/logout/')
        pesan = self.client.get('/lab-10/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Anda berhasil logout. Semua session Anda sudah dihapus", pesan)

    def test_get_data_user_di_cs_ui_helper(self):
    	username = "delfaag"
    	password = "delfa"
    	get_data_user(username, password)

    def test_omdb (self):
        x = "it"
        y = "2017"
        search_movie(x,y)

    def test_get_detail_movie(self):
    	x = "tt1396484"
    	get_detail_movie(x)

    