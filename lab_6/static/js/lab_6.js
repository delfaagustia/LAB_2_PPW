// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
  	print.value = "";
    /* implemetnasi clear all */
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } 
  else if (x === 'sin'){
  	print.value = Math.sin(print.value);
  }
  else if (x === 'tan'){
  	print.value = Math.tan (print.value);
  }
  else if (x === 'log'){
  	print.value = Math.log (print.value);
  }
  else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END


function chat(e){
    if(e.keyCode == 13){
    	var pesan = document.getElementById('isi-chat').value;
        document.getElementById('tarok-chat').value += pesan +"\n";
        document.getElementById('isi-chat').value = "";
        e.preventDefault();
    }
}

//untuk temanya ganti ganti
$(document).ready(function() {
	if (Storage) { // cek dulu ini 
		localStorage.themes = `[
			{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#8B0000"},
			{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#DB7093"},
			{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#800080"},
			{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#BDB76B"},
			{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#000080"},
			{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#7B68EE"},
			{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#ADFF2F"},
			{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#FFD700"},
			{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#006400"},
			{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#FF8C00"},
			{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#808000"}
		]`;
		//store nilai awal untuk selected theme default
		localStorage.selectedTheme = JSON.stringify({"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#0036f6"}); 
		// untuk mengatur tema awalnya indigo
		changeBodyTheme(JSON.parse(localStorage.selectedTheme));

		//cara kerja ini ga ngerti, nanti tanya asdos
		$('.my-select').select2({
			'data' : JSON.parse(localStorage.themes)
		});
	}
	else {
		console.log("eh ini gabisa browsernya, storage nya false")
	}
});

//ketika apply nya diklik dia akan ganti selected theme nya
$('.apply-button').on('click', function(){ 
    var selectedThemeId = $(".my-select").val();
    var selectedTheme = JSON.parse(localStorage.themes)[selectedThemeId];
    changeBodyTheme(selectedTheme);
    localStorage.selectedTheme = JSON.stringify(selectedTheme);
});

function changeBodyTheme(selectedTheme) {
	$('body').css({
    	"background" : selectedTheme.bcgColor,
    	"color" : selectedTheme.fontColor
    });
}
// END
