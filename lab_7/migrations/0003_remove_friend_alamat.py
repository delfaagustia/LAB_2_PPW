# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-11-13 23:07
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lab_7', '0002_friend_alamat'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='friend',
            name='alamat',
        ),
    ]
