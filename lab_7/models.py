from django.db import models

class Friend(models.Model):
    friend_name = models.CharField(max_length=400)
    npm = models.CharField(max_length=250)
    added_at = models.DateField(auto_now_add=True)
    alamat = models.CharField(max_length = 400,  default="-")    
    kodepos = models.CharField(max_length=50, default="0")
    kotalahir = models.CharField(max_length=200, default = "-")
    tgllahir = models.CharField(max_length=400, default="-")
    prodi = models.CharField(max_length=100, default="-")

