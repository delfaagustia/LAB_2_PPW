from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper

class Lab7UnitTest(TestCase):
	def test_lab7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)

	def test_lab7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)

	def test_lab7_template_using_lab7_html(self):
		response = Client().get('/lab-7/')
		self.assertTemplateUsed(response, "lab_7/lab_7.html")

	def test_get_friend_list_url_is_exist(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_add_friend(self):
		response_post = Client().post('/lab-7/add-friend/', 
			{'name':"zulia", 'npm':"1606833305", 'kodepos' : "19283", 'kotalahir' : "padang", 'alamat':"barel", 'tgllahir':"1998-08-09", 'prodi':"ilmu komputer"})
		self.assertEqual(response_post.status_code, 200)

	def test_validate_npm(self):
		response = self.client.post('/lab-7/validate-npm/')
		html_response = response.content.decode('utf8')
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(html_response, {'is_taken':False})

	def test_delete_friend(self):
		friend = Friend.objects.create(friend_name="zulia", npm="1606833305")
		response = Client().post('/lab-7/friend-list/delete-friend/' + str(friend.id) + '/')
		self.assertEqual(response.status_code, 302)
	
	def test_friend_biodata_url_is_exists(self):
		friend = Friend.objects.create(friend_name="zulia", npm="123")
		response = Client().get('/lab-7/biodata/123')
		self.assertEqual(response.status_code, 301)
		
	def test_biodata_template_using_biodata_html(self):
		friend = Friend.objects.create(friend_name="zulia", npm="1234")
		response = Client().get('/lab-7/biodata/1234/')
		self.assertTemplateUsed(response, "lab_7/biodata.html")

	def test_auth_param_dict(self):
		csui_helper = CSUIhelper()
		auth_param = csui_helper.instance.get_auth_param_dict()
		self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])
